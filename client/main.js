step = 0
console.debug('%s meteor hello', step++);

// I M P O R T S
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';
import { Accounts } from 'meteor/accounts-base';
import { Random } from 'meteor/random';

import './main.html';

// DEFAULTS / GLOBALS VARIABLES
const LANDING_PAGE = 'home';

// application defaults
app = {}; 
app.settings = {'timeout':30,'back':false,'next':true,'multi':false,'auto':false,'topic':true,'min':0,'max':1, 'rank':false };

// timers 
interval = {}; 
timer = {};
clock = {};
my = {};

Session.set('id', Random.id());

// COLLECTIONS
Meteor.subscribe('users');

Tests = new Meteor.Collection("tests");
Meteor.subscribe('tests');

Results = new Meteor.Collection("results");
Meteor.subscribe('results');

Lists = new Meteor.Collection('lists'); 
Meteor.subscribe('lists');

// GLOBAL FUNCTIONS
Number.prototype.clamp = function(min, max) {
  return Math.min(Math.max(this, min), max);
};

sum = (items, prop) => {
 return items.reduce( function(a, b){ return a + (parseInt(b[prop]) || 0)}, 0);
};

getList = (list, kind) => {
  
  entries = Lists.find({ kind : kind }, { sort: { rank: -1} }).fetch();
  extra=entries.map((function(item) {return item.title;}));
  cookie = kind.toUpperCase() + 'S'
  Session.set(cookie, _.unique(list.concat(extra).sort()));
    
  return Session.get(cookie);
}

resizeImage = (url, width, height, callback) => {
  var sourceImage = new Image();

  sourceImage.onload = function() {
    // Create a canvas with the desired dimensions
    var canvas = document.createElement("canvas");
    canvas.width = width;
    canvas.height = height;

    // Scale and draw the source image to the canvas
    canvas.getContext("2d").drawImage(sourceImage, 0, 0, width, height);

    // Convert the canvas to a data URL in PNG format
    callback(canvas.toDataURL());
  }

  sourceImage.src = url;
}

dateDiff = (a, b) => {
    var d = Math.abs(a.getTime() - b.getTime()) / 1000;                        // delta
    var r = {};                                                                // result
    var s = {                                                                  // struct
        year: 31536000,
        month: 2592000,
        week: 604800, // uncomment row to ignore
        day: 86400,   // feel free to add your own row
        hour: 3600,
        minute: 60,
        second: 1
    };

    Object.keys(s).forEach(function(key){
        r[key] = Math.floor(d / s[key]);
        d -= r[key] * s[key];
    });
    
    return r;
};

updateQuestion = (field, event, instance) => {
  var field = event.target;
  var value = $(field).val();

  my.test.questions[Session.get("Item")][field.id] = value;
  Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
}

updateScore = () => {
  takeId = Session.get('id');
  testId = my.test._id || 0;

  if (getSettings('multi')) {
    selected = Session.get('selected') || [];
    selected.forEach(function(item){
      record = {
          user: Meteor.userId()
        , test: testId
        , take: takeId
        , exam: 0
        , quiz: Session.get('Item')
        , pick: item
        , flag: Session.get('flagged') || false
        , time: Session.get('nao')
        , date: Session.get('time')
        , email: Meteor.user().profile.email
      }
      record.id = Results.insert(record);
      Meteor.call('updateScore', record);      
    })
  } else {
    record = {
        user: my.test.user
      , test: testId
      , take: takeId
      , exam: 0
      , quiz: Session.get('Item')
      , pick: Session.get('selected')
      , flag: Session.get('flagged') || false
      , time: Session.get('nao')
      , date: Session.get('time')
      , email: Meteor.user().profile.email
    }
    record.id = Results.insert(record);
    Meteor.call('updateScore', record);
  }
}

stopWatch = () => {
  Meteor.clearTimeout(timer);  
  clearTimeout(timer);
}

nextQuestion = () => {

  stopWatch();
  
  if (Session.get('Page') != 'test') return;

  updateScore();

  // set next 
  var last = my.test.questions.length-1  
  item = (Session.get('Item')+1).clamp(0, last);
  Session.set('Item', item);

  Session.set('selected', undefined);
  Session.set('flagged', undefined);

  timeout = getSettings('timeout')*1000;
  if (last == item) {
    d = new Date(new Date().getTime() + timeout); 
    Session.set('nao', d);
    timer = Meteor.setTimeout(function() { updateScore(); Session.set('Page', 'done');}, timeout); 
  } else {
    if (timeout){
      d = new Date(new Date().getTime() + timeout); 
      Session.set('nao', d);
      timer = Meteor.setTimeout(function() { nextQuestion(); }, timeout);
    }
  }
  
}

clickNext = () => {
  var item = Session.get('Item');
  var last = my.test.questions.length-1
  if (last == item) {
    updateScore(); 
    stopWatch();      
    Session.set('Page', 'done');
  } else {
    nextQuestion();
  }        
}

getSettings = (setting) => {
  console.debug('%s getSettings', step++);
  
  if (my.test) {
    var x = my.test.settings || app.settings;
    var y = my.test.questions[Session.get('Item')].settings || {};
    var z = $.extend(true, {}, x, y);
  
    if (setting) {
      return z[setting];
    } else {
      return z;
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// APP DATA
Template.registerHelper('equals', 
  (a, b) => {
    return (a === b);
  }
);

Template.registerHelper('Session', 
  (variable) => {
    return Session.get(variable);
  }
);

Template.registerHelper('initcaps', 
  (string) => {
    if (string) return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
  }
);

Template.registerHelper('offset', 
  (n) => { 
    n = n + 1
    return n;
  }
);

Template.registerHelper('scrollbar', 
  (height, rows, page) => {
    if (rows > page){
      return `height:${height}vh;width:100%;overflow:hidden;overflow-y:scroll;`;
    }
  }
);

Template.registerHelper('Page',
  () => {
    page = Session.get("Page");
    if (!page) {
      page = 'home'
    }
    //location.pathname = '/';
    //location.hash = '#' + page;
    return page;
  }
);

Template.registerHelper('avatar', 
  (email) => {
    if (email) {
      user = Meteor.users.findOne({ 'email': email });
      if (user){
        return user.profile.image || 'http://lorempixel.com/175/175/';
      }
    } else {
      return Meteor.user().profile.image || 'http://lorempixel.com/175/175/';
    }
  }
);

Template.registerHelper('fitText', 
  (string) => {
    if (string) {
      var size = 50 - ((string.length - 6)*6);
      var tag = '<span style="font-size:'+size.clamp(16, 64)+'px">'+string+'</span>'
      return tag;
    }
  }
);  

Template.registerHelper('score',
  () => {
    // map reduce sum aggregrates
    try {
      return Results.find({ take: Session.get('id') }).fetch().map(item => parseInt(item.points) || 0).reduce((a, b) => a + b);
    } catch (error) {
      console.error(error);
    } 
  }
);  

nao = () => {
  
  ts = Session.get("time") || new Date();                                                                                           // 459
  ft = Session.get('nao') || new Date();                                                                                            // 460
  r = dateDiff(ft, ts);
  if (ts < ft) {
    return (r.minute < 10 ? "0" : "") +  r.minute + ":" + (r.second < 10 ? "0" : "") + r.second;
  } else {
    return "Time up";
  }
  
}

Template.registerHelper('nao', nao);  

project = () => {
  console.debug('%s fetch project', step++);
  my.test = Tests.findOne(Session.get('Test'));
  return my.test;
};

questions = () => {
  console.debug('%s fetch questions', step++);
  my.test = Tests.findOne(Session.get('Test'));
  return my.test.questions;
};

Template.registerHelper('project', project);  
Template.registerHelper('questions', questions);  

///////////////////////////////////////////////////////////////////////////////
// BODY EVENTS
Template.body.created = () => { 

  console.debug('%s body created', step++);
}

Template.body.rendered = () => { 
  console.debug('%s body rendered', step++);

  if (Meteor.user() == null) {
    Accounts._loginButtonsSession.set('dropdownVisible', true);
    Session.set("Page", "home");
  } else {
    Session.set("Page", "test");
  }  

}

Template.home.created = () => { 

  console.debug('%s home created', step++);
}

Template.home.helpers({
  'tests':() => {
    console.debug('%s home helper tests', step++);
    my = Meteor.user().profile;
    my.tests = Tests.find({}, {sort: {expires: -1}}).fetch().reverse();
    Session.set('Test', my.tests[0]._id);
    return my.tests; 
  },
});

Template.home.rendered = () => { 

  console.debug('%s home rendered', step++);
}

Template.home.events({
  'click .test':(event, instance) => {
    console.debug('%s home click .test ', event.target.id);
    Session.set('Test', event.target.id);
  },  
  'click #clock':(event, instance) => {
    Session.set('Page', 'test');
  },
  'change input[type="file"]':(event,template) => {
    var files=event.target.files;
    if(files.length===0){
      return;
    }
    var file=files[0];
    var fileReader=new FileReader();
    fileReader.onload=function(event){
      var dataUri=event.target.result;
      resizeImage(dataUri, 175, 175, function(png){ 
        Meteor.users.update({_id: Meteor.userId()}, {$set: { "profile.image": png}});
      }); 
    };
    fileReader.readAsDataURL(file);
  },  
  'click .logout':(event, instance) => {
    event.preventDefault();
    Meteor.logout(function(error){
      if ( error ){
        alert('Error logging out: '+error); 
      } else {
        Object.keys(Session.keys).forEach(function(key){
          Session.set(key, undefined);
        });
        Session.keys = {}; // remove session keys
        Accounts._loginButtonsSession.set('dropdownVisible', true);  
      }
    });
  },    
  'change #email':(event, instance) => {
    Session.set('email', event.target.value);
  },
  'click #login':(event, instance) => {
    event.preventDefault();
    if (Session.get('email')){
      Meteor.call('getLogin', Session.get('email'), {returnStubValue: true});
    }
  },
});

///////////////////////////////////////////////////////////////////////////////
// TEST DATA
Template.test.helpers({
  'test':() => {
    console.debug('%s test helper test', step++);
    return my.test;    
  },
  'settings':() => {
    console.debug('%s test helper settings', step++);
    return getSettings();    
  },
  'selected':(occ) => {
    console.debug('%s test helper selected', step++);

    var selected = Session.get('selected') || [];
    if (getSettings('multi')) {
      if (selected.indexOf(String(occ)) == -1) { // Stringify occ here for '@index'
        return 'select'
      } else {
        return 'selected'        
      }
    } else {
      if (Session.get('selected') == occ) {
        return 'selected'
      } else {
        return 'select';
      }
    }
  }
});

// TEST EVENTS
Template.test.created = () => { 
  console.debug('%s test created', step++);

  Session.set('selected', undefined);
  Session.set('flagged', undefined);
  Session.set('Item', 0);
  
  my.test = Tests.findOne(Session.get('Test'));

  if (my.test) {
    timeout = getSettings('timeout')*1000;
    d = new Date(new Date().getTime() + timeout); 
    Session.set('nao', d);

    if (my.test.questions.length > 1) {
      timer = Meteor.setTimeout(function() { nextQuestion(); }, timeout);
    } else {
      timer = Meteor.setTimeout(function() { updateScore(); Session.set('Page', 'done');}, timeout);
    }
  }
}

Template.test.rendered = () => { 
  console.debug('%s test rendered', step++);
}

Template.test.destroyed = () => { 
  stopWatch();
  
  console.debug('%s test destroyed', step++);
}

Template.test.events({
  'click #back':(event, instance) => {
    var x = Session.get('Item')-1;
    Session.set('Item', x.clamp(0, my.test.questions.length));
  },  
  'click #next':(event, instance) => {
    var multi = getSettings('multi');
    if (multi) {
      if (getSettings('min')-1 < $('.selected').length && $('.selected').length < getSettings('max')+1) {
        nextQuestion();          
      } else {
        $('.answer').fadeTo(100, 0.3, function() { $(this).fadeTo(500, 1.0); }); 
      }
    } else {
      if (Session.get('selected')){
        nextQuestion();
      } else {
        $('.answer').fadeTo(100, 0.3, function() { $(this).fadeTo(500, 1.0); }); 
      }
    }
  },  
  'click #done':(event, instance) => {
    if (Session.get('selected')){
      updateScore();
      stopWatch();      
      Session.set('Page', 'done');
    } else {
      $('.answer').fadeTo(100, 0.3, function() { $(this).fadeTo(500, 1.0); });       
    }
  },  
  'click #clock':(event, instance) => {
    Session.set('flagged', true);
    clickNext();
  },  
  'click .option':(event, instance) => {
    if (nao != 'Time up') {
      var occ = event.target.id;

      var multi = getSettings('multi');
      if (multi) {
        selected = Session.get('selected') || [];
        if (selected.indexOf(occ) == -1) {
          selected.push(occ);
        } else {
          selected.splice(selected.indexOf(occ),1);
        } 
        Session.set('selected', selected);
      } else {
        Session.set('selected', occ);
      }

      var auto = getSettings('auto');
      if (auto) {
        if (getSettings('min')-1 < selected.length && selected.length < getSettings('max')+1) {
          clickNext();
        }                
      }
    }    
  },
});

Accounts.onLoginFromLink((error, response) => {
  console.debug('onLoginFromLink', step++);

  if (error) {
    page = 'home'
    Session.set('message', 'Sorry, you cannot take this test. Something went wrong. Your token has expired.');
    console.error(error);
    throw error;
  }
  user = Meteor.user(Meteor.userid());
  console.debug('%s Logged in!', user.email)
});

Template.done.created = () => { 

  Meteor.call("getTotal", Session.get('Test'), function (error, result) {
    console.debug('getTotal %s', result);
    Session.set("total", result);
  });
  
  console.debug('%s done created', step++);
}

Template.done.rendered = () => { 

  console.debug('%s done rendered', step++);
}

// DONE DATA
Template.done.helpers({
  'test':() => {
    console.debug('%s done helper test', step++);
    return my.test;    
  },
});

// DONE EVENTS
Template.done.events({
  'click .test':(event, instance) => {
    console.debug('%s done click .test', step++);
    Session.set("Page", 'home');
  },  
});

Meteor.startup(() => {
  console.debug('%s meteor startup', step++);

  Meteor.setInterval(function () {
    Meteor.call("getCurrentTime", function (error, result) {
      Session.set("time", new Date);
    });
  }, 1000);

});
