////////////////////////////////////////////////////////////////////////////////////////////////
//  T E S T B O B - c a n d i t a t e 
////////////////////////////////////////////////////////////////////////////////////////////////

import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Random } from 'meteor/random';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import sys from 'sys'
import { exec } from 'child_process';

////////////////////////////////////////////////////////////////////////////////////////////////

// Pass in the objects to merge as arguments.
// For a deep extend, set the first argument to `true`.
extend = function () {

    // Variables
    var extended = {};
    var deep = false;
    var i = 0;
    var length = arguments.length;

    // Check if a deep merge
    if ( Object.prototype.toString.call( arguments[0] ) === '[object Boolean]' ) {
        deep = arguments[0];
        i++;
    }

    // Merge the object into the extended object
    var merge = function (obj) {
        for ( var prop in obj ) {
            if ( Object.prototype.hasOwnProperty.call( obj, prop ) ) {
                // If deep merge and property is an object, merge properties
                if ( deep && Object.prototype.toString.call(obj[prop]) === '[object Object]' ) {
                    extended[prop] = extend( true, extended[prop], obj[prop] );
                } else {
                    extended[prop] = obj[prop];
                }
            }
        }
    };

    // Loop through each object and conduct a merge
    for ( ; i < length; i++ ) {
        var obj = arguments[i];
        merge(obj);
    }

    return extended;

};

// parse non-primitive  
withDates = (key, value) => {
  var a;
  if (typeof value === 'string') {
    a = /\/Date\((-?\d*)\)\//.exec(value);
    if (a) {
      return new Date(+a[1]);
    }
  }
  return value;
}

chocBox = (selection) => {
  return selection[Math.floor(Math.random() * selection.length)]
}

////////////////////////////////////////////////////////////////////////////////////////////////
Tests = new Mongo.Collection("tests");
Results = new Mongo.Collection("results");

// tests he taking not be making
Meteor.publish('tests', function () {
  if (this.userId) {
    me = Meteor.users.find({_id: this.userId}).fetch()[0];
    email = me.profile.email;
    now = new Date();
    return Tests.find( { members: { $all: [ { $elemMatch : { email: email } } ] }, tries: { $gt: 0}, expires: { $gte: now } } );
  }  
});

Meteor.publish('results', function () {
  if (this.userId) {
    me = Meteor.users.find({_id: this.userId}).fetch()[0];
    email = me.profile.email;
    return Results.find({'email': email});
  }    
});

Meteor.publish('users', function () {
  return Meteor.users.find();
});

Meteor.publish(null, function() {
  return Meteor.users.find({}, {fields: {username: 1, name: 1, profile: 1}});
});  

Accounts.config({
  sendVerificationEmail: true,
  forbidClientAccountCreation: false
});

Accounts.emailTemplates.siteName = "testBob";
Accounts.emailTemplates.from = "testBob Admin <accounts@oznz.me>";
Accounts.emailTemplates.enrollAccount.subject = function (user) {
    return "Welcome to testBob, " + user.profile.name;
};
Accounts.emailTemplates.enrollAccount.text = function (user, url) {
   return "You have been selected to take a test!"
     + " To activate your account, simply click the link below:\n\n"
     + url;
};
Accounts.emailTemplates.resetPassword.from = function () {
   // Overrides value set in Accounts.emailTemplates.from when resetting passwords
   return "testBob Password Reset <no-reply@oznz.me>";
};

///  T R I G G E R S  ==================================================
Accounts.onCreateUser(function(options, user) {
    
    if (user.username == 0) {
      user.username = user.emails[0].address.split('@')[0].replace('.','');
    }
    
    var userProperties = {
      profile: options.profile || {},
      isAdmin: user.isAdmin
    };
    
    user = _.extend(user, userProperties);
        
    if (!Meteor.users.find().count() ) {
      user.isAdmin = true;
    }
  
    if (options.profile) {
      user.profile = options.profile;
    }
    
    if (options.email) {
      user.profile.email = options.email;
    }
    
    if (!user.profile.name) {
      user.profile.name = user.username;
    }

    user.profile.exams = [];
    user.profile.teams = [];
    user.profile.tests = [];
    
    user.profile.joined = new Date();

    return user;
    
});

// Permissions
Meteor.users.allow({
  insert: function (userId, doc) {
    // only admin can insert 
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  },
  update: function (userId, doc, fields, modifier) {
    if (userId && doc._id === userId) {
      console.log("user allowed to modify own account!");
      // user can modify own 
      return true;
    }
    // admin can modify any
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  },
  remove: function (userId, doc) {
    // only admin can remove
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  }
});

// Permissions
Results.allow({
  insert: function (userId, doc) {
    // only admin can insert 
    var u = Meteor.users.findOne({_id:userId});
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  }
});

Tests.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    console.log('Updating a test');
    return true;
  },
  remove: function (userId, doc) {
    return true;
  }
});


///  S E R V I C E S   =================================================
Meteor.methods({
  'version':() => {
    console.log("testBob 0.99");
  },
  'createNewUser': (username, email) => {
    var userId = Accounts.createUser({username: username, email: email, password: Random.id()});
    Accounts.sendEnrollmentEmail(userId);
  },
  'changeAvatar':(userId, imageURL) => {
    return Meteor.users.update({_id: Meteor.userId()}, {$set: { "profile.image": imageURL}});
  },  
  'setUser':(userId, newValue) => {
    return Meteor.users.update({_id: userId}, {$set: newValue });
  },
  'getLogin': (email) => {
    var user = Meteor.users.find({'profile.email': email}).fetch()[0];
    if (user) {
      userId = user._id;
      console.log('Getting login for user %s', userId);
      Accounts.sendLoginEmail(userId, email);
    } else {
      var userId = Accounts.createUser({username: email.split('@')[0], email: email, password: Random.id()});
      Accounts.sendEnrollmentEmail(userId);
      console.log('New user %s', userId);
    }
  },
  'getCurrentTime':() => {
    //console.log('on server, getCurrentTime called');
    return new Date();
  },
  'getTotal': (testId) => {

    test = Tests.findOne(testId);
    var total = 0;
    if (test) {
      test.questions.forEach(
        (question) => {
          question.answers.forEach(
          (answer) => { 
            console.log("Answer %s %s", answer.option, parseInt(answer.points));
            total += parseInt(answer.points);
          });
        })
      
      test.tries = (test.tries === undefined) ? 0 : test.tries; 
      if (test.tries > 0) {
        test.tries-- 
        Tests.update({'_id': testId},{ $set: { 'tries': test.tries }});
      }
      console.log('Total %s', total);      
      return total;
    }
  },
  'removeScores':() => {
    console.log("Goodbye cruel world");
    return Results.remove({});
  },
  'loadTests':(filename) => {

    fs = Npm.require('fs');
    var data = fs.readFileSync(process.env["PWD"] + "/public/"+filename, "UTF-8");
    var test = JSON.parse(data, withDates);
    delete test._id; 
    test.expires = moment(test.expires, "YYYY-MM-DD" ).toDate();
    Tests.insert(test); // i.e. a copy
    console.log(" new test :: %s", test);    

  },
  'dumpTests':() => {

    fs = Npm.require('fs');

    tests = Tests.find().fetch();
    tests.forEach(function(test){      
      record = JSON.stringify(test, null, 2);
      fs.writeFile(process.env["PWD"] + "/public/"+test.name+".txt", record, 
        function (error) {
          if (error) throw error;
          console.log('Tests!');
        }
      );
    });

  },
  'removeTests':(testId) => {
    console.log("Parting is such sweet sorrow");
    if (testId) {
      return Tests.remove({"_id": testId});
    } else {
      return Tests.remove({});      
    }
  },
  'showTests':(testId) => {
    if (testId) {
      tests = Tests.find({"_id": testId}).fetch();
    } else {
      tests = Tests.find({}).fetch();
    }
    return tests;
  },
  'updateTests':(fields) => {
    console.log('%s setting fields ', JSON.stringify(fields));
    all = Tests.find({}).fetch();
    all.forEach((item) => { 
      Tests.update({'_id': item._id }, {'$set': fields } );
    });
  },
  'updateScore':(record) => {
    try {
      // The Record 
      x = Results.find({ _id: record.id }).fetch()[0];      
      console.log('x::'+JSON.stringify(x));
      try {
        var test = Tests.find(record.test).fetch()[0];
      } catch (error) {
        console.error(error.message);
      }
      y = test.questions[record.quiz].answers[record.pick];

      result = extend(true, {}, x, y);
      console.log('z::'+JSON.stringify(result));

      delete result._id; //to avoid update _id minimongo
      Results.update({ _id: record.id }, {$set: result });

      // sum aggregates
      alles = Results.find({ take: record.take }).fetch().map(item => parseInt(item.points) || 0).reduce((a, b) => a + b);
      console.log(JSON.stringify(alles, null, 2));
    }
    catch(error) {
      console.error(error.message);
    }
  },
  'removeResults':(takeId) => {
    console.log("Nothing to see here");
    if (takeId) {
      return Results.remove({"take": takeId});
    } else {
      return Results.remove({}); 
    }
  },
});
